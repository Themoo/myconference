package com.nachiengmai.moo.myconference;

import java.io.Serializable;

/**
 * Created by wacharapongnachiengmai on 3/15/16 AD.
 */
public class Conference implements Serializable {
    private int conferenceId;
    private String conferenceName;
    private String conferencePlace;
    private String conferenceDate;
    private Category category;

    public int getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(int conferenceId) {
        this.conferenceId = conferenceId;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public void setConferenceName(String conferenceName) {
        this.conferenceName = conferenceName;
    }

    public String getConferencePlace() {
        return conferencePlace;
    }

    public void setConferencePlace(String conferencePlace) {
        this.conferencePlace = conferencePlace;
    }

    public String getConferenceDate() {
        return conferenceDate;
    }

    public void setConferenceDate(String conferenceDate) {
        this.conferenceDate = conferenceDate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getThaiDate(){
        String thaiYear, thaiMonth, thaiDay;
        int thaiYearCal = 0;
        String[] thaiMonthName = {"", "มค.", "กพ.", "มีค.", "เมย.", "พค.", "มิย.", "กค.", "สค.", "กย.", "ตค.", "พย.", "ธค."};
        thaiYear = this.conferenceDate.substring(0,4);
        thaiMonth = this.conferenceDate.substring(5,7);
        thaiDay = this.conferenceDate.substring(8,10);
        thaiYearCal = Integer.valueOf(thaiYear)+ 543;
        return Integer.valueOf(thaiDay)+" "+thaiMonthName[Integer.valueOf(thaiMonth)]+" "+ thaiYearCal;
    }

    @Override
    public String toString() {
        return "Conference{" +
                "conferenceId=" + conferenceId +
                ", conferenceName='" + conferenceName + '\'' +
                ", conferencePlace='" + conferencePlace + '\'' +
                ", conferenceDate='" + conferenceDate + '\'' +
                ", category=" + category +
                '}';
    }
}
