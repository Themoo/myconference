package com.nachiengmai.moo.myconference;

import java.io.Serializable;

/**
 * Created by wacharapongnachiengmai on 3/15/16 AD.
 */
public class Category implements Serializable {
    private int categoryId;
    private String categoryName;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
