package com.nachiengmai.moo.myconference;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {
    TextView confId, confName, confPlace, confDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        confId = (TextView) findViewById(R.id.conf_id);
        confName = (TextView) findViewById(R.id.conf_name);
        confPlace = (TextView) findViewById(R.id.conf_place);
        confDate = (TextView) findViewById(R.id.conf_date);

        final Conference conference = (Conference) getIntent().getSerializableExtra("confObj");
        confId.setText(String.valueOf(conference.getConferenceId()));
        confName.setText(String.valueOf(conference.getConferenceName()));
        confPlace.setText(String.valueOf(conference.getConferencePlace()));
        confDate.setText(String.valueOf(conference.getThaiDate()));

        Button delBtn = (Button) findViewById(R.id.del_btn);
        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConferenceDAO conferenceDAO = new ConferenceDAO(getApplicationContext());
                conferenceDAO.delete(conference);
                conferenceDAO.close();
                finish();
            }
        });

    }
}
