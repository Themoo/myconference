package com.nachiengmai.moo.myconference;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by wacharapongnachiengmai on 3/22/16 AD.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String databaseName = "myconference.sqlite";
    private static final int databaseVersion = 4;
    Context myContext;


    public DbHelper(Context context) {

        super(context, databaseName, null, databaseVersion);
        this.myContext = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("Moo1", "Table All");
        //สร้างตาราง conference
        String sqlText = "CREATE TABLE conference (" +
                "conference_id INTEGER PRIMARY KEY, " +
                "conference_name TEXT, " +
                "conference_place TEXT, " +
                "conference_date TEXT, " +
                "category_id INTEGER" +
                ");";
        db.execSQL(sqlText);

        //สร้างตาราง category
        sqlText = "CREATE TABLE category (" +
                "category_id INTEGER PRIMARY KEY, " +
                "category_name TEXT" +
                ");";
        db.execSQL(sqlText);

        //ใส่ข้อมูลเริ่มต้นในตาราง category
        sqlText = "INSERT INTO category (category_id, category_name) VALUES (" +
                "1, 'วิศวกรรม');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO category (category_id, category_name) VALUES (" +
                "2, 'วิทยาศาสตร์และเทคโนโลยี');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO category (category_id, category_name) VALUES (" +
                "3, 'วิทยาศาสตร์สุขภาพ');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO category (category_id, category_name) VALUES (" +
                "4, 'บริหารธุรกิจ');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO category (category_id, category_name) VALUES (" +
                "5, 'มนุษยศาสตร์และสังคมศาสตร์');";
        db.execSQL(sqlText);

        //ใส่ข้อมูลเริ่มต้นในตาราง conference
        sqlText = "INSERT INTO conference (conference_id, conference_name, conference_place, " +
                "conference_date, category_id) VALUES (" +
                "1, 'conference1 name', 'conference1 place', '2016-01-01', 1);";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO conference (conference_id, conference_name, conference_place, " +
                "conference_date, category_id) VALUES (" +
                "2, 'conference2 name', 'conference2 place', '2016-02-02', 2);";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO conference (conference_id, conference_name, conference_place, " +
                "conference_date, category_id) VALUES (" +
                "3, 'conference3 name', 'conference3 place', '2016-03-03', 3);";
        db.execSQL(sqlText);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DELETE FROM conference;");

        String sqlText = null;

        sqlText = "INSERT INTO conference (conference_id, conference_name, conference_place, " +
                "conference_date, category_id) VALUES (" +
                "1, 'conference1 name', 'conference1 place', '2016-01-01', 1);";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO conference (conference_id, conference_name, conference_place, " +
                "conference_date, category_id) VALUES (" +
                "2, 'conference2 name', 'conference2 place', '2016-02-02', 2);";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO conference (conference_id, conference_name, conference_place, " +
                "conference_date, category_id) VALUES (" +
                "3, 'conference3 name', 'conference3 place', '2016-03-03', 3);";
        db.execSQL(sqlText);

    }
}
