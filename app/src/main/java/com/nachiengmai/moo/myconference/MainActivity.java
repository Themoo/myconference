package com.nachiengmai.moo.myconference;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView confListView;
    private ArrayList<Conference> conferences;
    private ConferenceListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.fab:
                        Intent addConfIntent = new Intent(MainActivity.this, AddConfActivity.class);
                        startActivity(addConfIntent);
                }
            }
        });

        confListView = (ListView)findViewById(R.id.conf_list_view);


        updateListview();

        confListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(MainActivity.this, DetailActivity.class);
                detailIntent.putExtra("confObj", adapter.getItem(position));
                startActivity(detailIntent);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        updateListview();
    }

    private void updateListview(){
        //create ArrayList

        conferences = new ArrayList<Conference>();
        ConferenceDAO conferenceDAO = new ConferenceDAO(getApplicationContext());
        conferences = conferenceDAO.findAll();
        conferenceDAO.close();

        //set adapter
        adapter = new ConferenceListAdapter(this, conferences);

        confListView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            Intent addConfIntent = new Intent(this, AddConfActivity.class);
            startActivity(addConfIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
