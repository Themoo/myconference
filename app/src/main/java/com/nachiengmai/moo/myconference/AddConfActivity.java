package com.nachiengmai.moo.myconference;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class AddConfActivity extends AppCompatActivity {
    private EditText confId, confName, confPlace, confDate, cateId;
    private Button addBtn;
    private DatePicker datePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_conf);

        confId = (EditText) findViewById(R.id.conference_id);
        confName = (EditText) findViewById(R.id.conference_name);
        confPlace = (EditText) findViewById(R.id.conference_place);
        confDate = (EditText) findViewById(R.id.conference_date);
        cateId = (EditText) findViewById(R.id.conference_category);

        datePicker = (DatePicker) findViewById(R.id.conference_date_picker);

        addBtn = (Button) findViewById(R.id.add_btn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Conference conference = new Conference();
//                conference.setConferenceId(Integer.valueOf(String.valueOf(confId.getText())));
//                conference.setConferenceName(String.valueOf(confName.getText()));
//                conference.setConferencePlace(String.valueOf(confPlace.getText()));
//                conference.setConferenceDate(String.valueOf(confDate.getText()));
//                conference.setCategory(new Category());
//
//                ConferenceDAO conferenceDAO = new ConferenceDAO(AddConfActivity.this);
//                conferenceDAO.add(conference);
//                conferenceDAO.close();
//                finish();
                String cDate = String.valueOf(datePicker.getYear()) + String.valueOf(datePicker.getMonth()) + String.valueOf(datePicker.getDayOfMonth());
                Toast.makeText(AddConfActivity.this, cDate,Toast.LENGTH_SHORT).show();
            }
        });

    }
}
