package com.nachiengmai.moo.myconference;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 3/22/16 AD.
 */
public class ConferenceDAO {
    SQLiteDatabase database;
    DbHelper dbHelper;
    Context context;

    public ConferenceDAO(Context context) {
        dbHelper = new DbHelper(context);
        database = dbHelper.getReadableDatabase();
        this.context = context;
    }

    public void close(){
        database.close();
    }

    public ArrayList<Conference> findAll(){
        ArrayList<Conference> resultArrayList = new ArrayList<Conference>();
        String SQLText = "SELECT * FROM conference ORDER BY conference_id DESC;";
        Cursor cursor = this.database.rawQuery(SQLText, null);
        cursor.moveToFirst();
        Conference conference;
        CategoryDAO categoryDAO = new CategoryDAO(this.context);
        while (!cursor.isAfterLast()){
            conference = new Conference();
            conference.setConferenceId(cursor.getInt(0));
            conference.setConferenceName(cursor.getString(1));
            conference.setConferencePlace(cursor.getString(2));
            conference.setConferenceDate(cursor.getString(3));
            conference.setCategory(categoryDAO.findById(cursor.getInt(4)));
            resultArrayList.add(conference);
            cursor.moveToNext();

        }
        categoryDAO.close();
        return resultArrayList;
    }

    public void add(Conference conference){
        Conference myConference = conference;
        String sqlText = "INSERT INTO conference (conference_id, conference_name, conference_place, conference_date, category_id) VALUES ("+myConference.getConferenceId()+", '"+myConference.getConferenceName()+"', '"+myConference.getConferencePlace()+"', '"+myConference.getConferenceDate()+"', 1);";

        database.execSQL(sqlText);
    }

    public void delete(Conference conference){
        String sqlText = "DELETE FROM conference WHERE conference_id=" + conference.getConferenceId();
        database.execSQL(sqlText);
    }
}
