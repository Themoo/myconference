package com.nachiengmai.moo.myconference;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 3/15/16 AD.
 */
public class ConferenceListAdapter extends BaseAdapter {

    private static Activity activity;
    private static LayoutInflater layoutInflater;
    private ArrayList<Conference> conferences;

    public ConferenceListAdapter(Activity activity, ArrayList<Conference> conferences) {
        this.activity = activity;
        this.conferences = conferences;
        this.layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return conferences.size();
    }

    @Override
    public Conference getItem(int position) {
        return conferences.get(position);
    }

    @Override
    public long getItemId(int position) {
        return conferences.get(position).getConferenceId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        v = layoutInflater.inflate(R.layout.conference_layout, null);
        TextView confName = (TextView)v.findViewById(R.id.conf_name);
        TextView confPlace = (TextView)v.findViewById(R.id.conf_place);
        TextView confDate = (TextView)v.findViewById(R.id.conf_date);
        //ดึงค่าจาก conferenced ใส่ Textview
        confName.setText(conferences.get(position).getConferenceName());
        confPlace.setText(conferences.get(position).getConferencePlace());
        confDate.setText(conferences.get(position).getThaiDate());

        return v;
    }
}
