package com.nachiengmai.moo.myconference;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by wacharapongnachiengmai on 3/22/16 AD.
 */
public class CategoryDAO {
    SQLiteDatabase database;
    DbHelper dbHelper;

    public CategoryDAO(Context context) {
        dbHelper = new DbHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        database.close();
    }

    public Category findById(int categoryId){
        Category resultCategory = new Category();
        String sqlText = "SELECT * FROM category WHERE category_id=" + categoryId;
        Cursor cursor = this.database.rawQuery(sqlText, null);
        cursor.moveToFirst();
        if(cursor.getCount() != 0) {
            resultCategory.setCategoryId(cursor.getInt(0));
            resultCategory.setCategoryName(cursor.getString(1));
        }
//        resultCategory.setCategoryId(1);
//        resultCategory.setCategoryName("cate1");
        return resultCategory;
    }
}
